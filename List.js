/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  ListView,
} from 'react-native';

export default class List extends Component {

	
	constructor(props) {
		super(props);

		this.ds = new ListView.DataSource({ rowHasChanged: (r1: Object, r2: Object) => { return r1 !== r2; } });

		this.state = {
			dataSource: this.ds.cloneWithRows([{ value: 1 }]),
		}
	};

	_renderRow(rowData) {
		console.log('RenderRow: ', rowData.value);
		return (
			<View>
				<Text>{rowData.value}</Text>
			</View>
		)
	}
	
	componentDidMount() {
		setTimeout(() => {
			const data = Array.from({ length: 2000 }).map((_, index) => {
				return { value: index };
			})
			this.setState({
				dataSource: this.ds.cloneWithRows(data),
			})
		}, 3000)
	}
	
	render() {
		return (
			<View>
				<ListView
					dataSource={this.state.dataSource}
					enableEmptySections
					removeClippedSubviews={false}
					renderRow={this._renderRow}
				/>
			</View>
		);
	}
}

